﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnTestWPF.Data
{
    public enum ItemType { WALL = 1, WEAKWALL = 2, TANK = 3, BULLET = 4, ROCKET = 5, AMMOCRATE = 6 }
    public abstract class GameObject
    {
        public uint ItemID { get; private set; }
        public ItemType ItemType { get; private set; }
        public char ItemChar { get; private set; }
        public int OwnerID { get; private set; }
        public double X { get; private set; }
        public double Y { get; private set; }
        public int Rot { get; private set; }
        public int Speed { get; private set; }
        public int BulletNum { get; private set; }
        public int RocketNum { get; private set; }
        public DateTime CreationDate { get; private set; } = DateTime.Now;
        public double Angle { get; set; }
        public double Distance { get; set; }

        public GameObject(uint ItemID, ItemType ItemType, char ItemChar, int OwnerID, double X, double Y, int Rot, int Speed, int BulletNum, int RocketNum)
        {
            this.ItemID = ItemID;
            this.ItemType = ItemType;
            this.ItemChar = ItemChar;
            this.OwnerID = OwnerID;
            this.X = X;
            this.Y = Y;
            this.Rot = Rot;
            this.Speed = Speed;
            this.BulletNum = BulletNum;
            this.RocketNum = RocketNum;
        }

        //public GameObject()
        //{

        //}

        public override string ToString()
        {
            return string.Format
                ("{0}: ID={1}, Char={2}, Pos=({3};{4}), Rot={5}, Speed={6}, BulletNum={7}, RocketNum={8} (OwnerID={9})",
                ItemType, ItemID, ItemChar,    X , Y,   Rot,     Speed,     BulletNum,     RocketNum,     OwnerID);
        }

        //public double GetDistance(GameObject o2)
        //{
        //    return Math.Sqrt(Math.Pow((o2.X - this.X), 2) + Math.Pow((o2.Y - this.Y), 2));
        //}
        //public double GetAngle(GameObject item)
        //{
        //    double xDiff = (double)(item.X - this.X);
        //    double yDiff = (double)(item.Y - this.Y);
        //    double angleDeg = Math.Atan(yDiff / xDiff) * 180 / Math.PI;

        //    int siknegyed = 0;
        //    if (xDiff > 0 && yDiff < 0)
        //    {
        //        siknegyed = 1;
        //        angleDeg += 360;
        //        angleDeg += 6;
        //        angleDeg = angleDeg > 360 ? angleDeg - 360 : angleDeg;
        //    }
        //    else if (xDiff < 0 && yDiff < 0)
        //    {
        //        siknegyed = 2;
        //        angleDeg += 180;
        //        angleDeg += 6;
        //    }
        //    else if (xDiff < 0 && yDiff > 0)
        //    {
        //        siknegyed = 3;
        //        angleDeg += 180;
        //        angleDeg -= 6;
        //    }
        //    else if (xDiff > 0 && yDiff > 0)
        //    {
        //        siknegyed = 4;
        //        angleDeg -= 6;
        //        angleDeg = angleDeg < 0 ? angleDeg + 360 : angleDeg;
        //    }

        //    if (xDiff == 0 && yDiff < 0)
        //        angleDeg = 270;
        //    else if (xDiff == 0 && yDiff > 0)
        //        angleDeg = 90;
        //    else if (yDiff == 0 && xDiff > 0)
        //        angleDeg = 0;
        //    else if (yDiff == 0 && xDiff < 0)
        //        angleDeg = 180;

        //    //Debug.WriteLine("----");
        //    //Debug.WriteLine(MyPlayer);
        //    //Debug.WriteLine(item);
        //    //Debug.WriteLine(angleDeg);

        //    int dist = 5;
        //    if (angleDeg < 270 + dist && angleDeg > 270 - dist)
        //        angleDeg = 270;
        //    else if (angleDeg > 360 - dist && angleDeg < 0 + dist)
        //        angleDeg = 0;
        //    else if (angleDeg < 90 + dist && angleDeg > 90 - dist)
        //        angleDeg = 90;
        //    else if (angleDeg < 180 + dist && angleDeg > 180 - dist)
        //        angleDeg = 180;

        //    return angleDeg;
        //}

    }
}