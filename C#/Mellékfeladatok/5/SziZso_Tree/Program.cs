﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SziZso_Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            int elsoSzam = int.Parse(Console.ReadLine());
            Tree.KezdoSzam = int.Parse(Console.ReadLine());
            Tree.KezdoSzamOsztoi = Tree.GetOsztok_ÖnmagátKivéve(Tree.KezdoSzam);
            Console.ReadLine(); // osztók, én számolom, nem érdekel

            if (elsoSzam == 2)
            {
                HandleNew();
            }
            else if (elsoSzam == 4)
            {
                HandleOld();
            }
            Console.ReadLine();
        }
        static void HandleNew()
        {
            // Do calculations
            List<Tree> trees = Tree.AutoGenerateStartingTrees();
            BuildTree();
            Tree winningTree = trees.Find(s => s.RootNode.Sign.Value == true);
            //Tree.DepthFirstTraversal(winningTree);
            Console.WriteLine(winningTree.RootNode.Value);
        }
        static void HandleOld()
        {
            string[] lepesek = Console.ReadLine().Split(' ');
            Console.ReadLine(); // utolsó lépése, de már benne van az előzőben is => feldolgoztam
            List<Tree> trees = Tree.AutoGenerateStartingTrees();
            BuildTree();

            Tree currentTree = trees.Find(s => s.RootNode.Value == int.Parse(lepesek[0]));
            //Tree.DepthFirstTraversal(currentTree);
            Node CurrentNode = currentTree.RootNode;
            for (int i = 1; i < lepesek.Length; i++)
            {
                CurrentNode = CurrentNode.Children.Find(s => s.Value == int.Parse(lepesek[i]));
            }
            Node winningNode = CurrentNode.Children.Find(s => s.Sign.Value == true);
            if (winningNode != null)
                Console.WriteLine(winningNode.Value);
            else
                Console.WriteLine("Already ended");
        }

        static void BuildTree()
        {
            List<List<Node>> x = Node.AllDepthNodes;
            for (int depth = x.Count - 1; depth >= 0; depth--)
            {
                List<Node> NodesOnDepth = x[depth];
                foreach (Node node in NodesOnDepth)
                {
                    if (node.Children.Count == 0)
                        continue;

                    if (node.Children.Count == 1)
                    {
                        node.Sign = node.Children[0].Sign;
                    }
                    else // 2 or more children
                    {
                        if (node.Depth % 2 == 1)
                        { 
                            // AND
                            node.Sign = true;
                            foreach (Node child in node.Children)
                            {
                                if (child.Sign.Value == false)
                                {
                                    node.Sign = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            // OR
                            node.Sign = false;
                            foreach (Node child in node.Children)
                            {
                                if (child.Sign.Value == true)
                                {
                                    node.Sign = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}