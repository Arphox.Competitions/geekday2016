﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using TankCommTest.Communication;

namespace ConnTestWPF.Data
{
    public class DataHandler
    {
        ulong counter = 0;

        public List<GameObject>[,] Map { get; private set; }

        public List<Player> Enemies { get; private set; }
        public List<Wall> Walls { get; private set; }

        public Player MyPlayer { get; private set; }
        Point PreviousOtherPlayerPoint { get; set; }
        DateTime PreviousOtherPlayerPointTime { get; set; }

        public int MyPlayerID { get; private set; } = int.MinValue;
        int TimeLeft;

        bool LastResortDONE = false;

        DispatcherTimer actionTimer = new DispatcherTimer();

        List<double> PacketProcessTimes = new List<double>();
        MainWindow MainWindowObj;
        Communicator Communicator;

        public DataHandler(MainWindow MainWindowObj, Communicator communicator)
        {
            this.Communicator = communicator;
            this.MainWindowObj = MainWindowObj;

            Map = new List<GameObject>[20, 20];
            Enemies = new List<Player>();
            Walls = new List<Wall>();
            Init_MapElementsToEmptyList();

            actionTimer.Interval = new TimeSpan(0, 0, 0, 0, 750);
            actionTimer.Tick += ActionTimer_Tick;
            actionTimer.Start();
        }

        //private void CalculateAnglesAndDifferences()
        //{
        //    for (int i = 0; i < Map.GetLength(0); i++)
        //    {
        //        for (int j = 0; j < Map.GetLength(0); j++)
        //        {
        //            if (Map[i, j] != null)
        //            {
        //                foreach (GameObject item in Map[i, j])
        //                {
        //                    item.Distance = MyPlayer.GetDistance(item);
        //                    item.Angle = MyPlayer.GetAngle(item);
        //                }
        //            }
        //        }
        //    }
        //}
        private void ActionTimer_Tick(object sender, EventArgs e)
        {
            counter++;
            if (MyPlayer == null)
                return;

            List<int> clearAngles = FindClearPathAngle();

            ShootIfStuck();
            if (clearAngles.Count > 0)
            {
                if (clearAngles.Any(s => s == MyPlayer.Rot))
                {
                    Communicator.SendCommand(CommandType.SETROTATION, (byte)(clearAngles.Find(s => s == MyPlayer.Rot) / 10));
                }
                else if (clearAngles.Count >= 2 && clearAngles.Any(s => s == Math.Abs(MyPlayer.Rot - 180)))
                {
                    clearAngles.Remove(Math.Abs(MyPlayer.Rot - 180));
                    Communicator.SendCommand(CommandType.SETROTATION, (byte)(clearAngles[0] / 10));
                }
                else
                {
                    Communicator.SendCommand(CommandType.SETROTATION, (byte)(clearAngles[0] / 10));
                }
                Communicator.SendCommand(CommandType.SETSPEED, 200);
            }
            ShootIfEnemyFrontOfMe();
            Communicator.SendCommand(CommandType.SETSPEED, 200);

            LastResort();
            ShootUnnecessaryBullets();
        }


        private List<int> FindClearPathAngle()
        {
            List<int> clears = new List<int>();
            if (MyPlayer == null)
                return clears;

            if (Map[MyPlayer.Yrounded, MyPlayer.Xrounded - 1].Count == 0 ||
                Map[MyPlayer.Yrounded, MyPlayer.Xrounded - 1].Any(s => s is AmmoCrate)) // fel
                clears.Add(180);
            if (Map[MyPlayer.Yrounded, MyPlayer.Xrounded + 1].Count == 0 ||
                Map[MyPlayer.Yrounded, MyPlayer.Xrounded + 1].Any(s => s is AmmoCrate)) // le
                clears.Add(0);
            if (Map[MyPlayer.Yrounded + 1, MyPlayer.Xrounded].Count == 0 ||
                Map[MyPlayer.Yrounded + 1, MyPlayer.Xrounded].Any(s => s is AmmoCrate))  // jobbra
                clears.Add(90);
            if (Map[MyPlayer.Yrounded - 1, MyPlayer.Xrounded].Count == 0 ||
                Map[MyPlayer.Yrounded - 1, MyPlayer.Xrounded].Any(s => s is AmmoCrate)) // balra
                clears.Add(270);

            return clears;
        }
        private void ShootIfEnemyFrontOfMe()
        {
            if (Map[MyPlayer.Yrounded, MyPlayer.Xrounded - 1].Any(s => s is Player)) // fel
            {//180
                Communicator.SendCommand(CommandType.SETROTATION, 18);
                if (MyPlayer.CanShootRocket())
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
            }
            if (Map[MyPlayer.Yrounded, MyPlayer.Xrounded + 1].Any(s => s is Player)) // le
            {//0
                Communicator.SendCommand(CommandType.SETROTATION, 0);
                if (MyPlayer.CanShootRocket())
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
            }
            if (Map[MyPlayer.Yrounded + 1, MyPlayer.Xrounded].Any(s => s is Player))  // jobbra
            {//90
                Communicator.SendCommand(CommandType.SETROTATION, 9);
                if (MyPlayer.CanShootRocket())
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
            }
            if (Map[MyPlayer.Yrounded - 1, MyPlayer.Xrounded].Any(s => s is Player)) // balra
            {
                Communicator.SendCommand(CommandType.SETROTATION, 27);
                if (MyPlayer.CanShootRocket())
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
            }
        }
        private void ShootIfStuck()
        {
            if (DateTime.Now.Subtract(PreviousOtherPlayerPointTime) > new TimeSpan(0, 0, 2))
            {
                if (MyPlayer.BulletNum > 0)
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                PreviousOtherPlayerPointTime = DateTime.Now;
            }
        }
        private void LastResort()
        {
            if (LastResortDONE == true)
                return;

            if (TimeLeft < 15)
            {
                LastResortDONE = true;
                Communicator.SendCommand(CommandType.SETSPEED, 100);
                int normal = MyPlayer.BulletNum;
                int rocket = MyPlayer.RocketNum;
                int sum = normal + rocket;
                int angle = 0;
                for (int i = 0; i < sum; i++)
                {
                    if (normal > 0)
                    {
                        Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
                        normal--;
                    }
                    else if (rocket > 0)
                    {
                        Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
                        rocket--;
                    }
                    angle += 360 / sum;
                    Thread.Sleep(100);
                    Communicator.SendCommand(CommandType.SETROTATION, (byte)(angle / 10)); ;
                }
            }
        }
        private void ShootUnnecessaryBullets()
        {
            int normal = MyPlayer.BulletNum;
            int rocket = MyPlayer.RocketNum;
            int sum = normal + rocket;

            if (sum > 6 && counter % 5 == 0)
            {
                if (normal > 0)
                    Communicator.SendCommand(CommandType.SHOOTBULLET, 0);
                else
                    Communicator.SendCommand(CommandType.SHOOTROCKET, 0);
            }
        }

        private void Init_MapElementsToEmptyList()
        {
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    Map[i, j] = new List<GameObject>(0);
                }
            }
        }
        public void ProcessPacket(byte[] packetArray)
        {
            Stopwatch sw = Stopwatch.StartNew();
            uint ItemID = (uint)(packetArray[0] * 16777216 + packetArray[1] * 65536 + packetArray[2] * 256 + packetArray[3]);
            if (ItemID == uint.MaxValue)
            {
                HandleStatusPacket(packetArray);
            }
            else
            {
                HandleItemPacket(packetArray, ItemID);
            }
            PacketProcessTimes.Add(sw.ElapsedMilliseconds);
        }
        private void HandleStatusPacket(byte[] packetArray)
        {
            int PlayerID = packetArray[5];
            MyPlayerID = PlayerID;

            if (TimeLeft != packetArray[6]) // If changed (1 sec delayed)
            {
                TimeLeft = packetArray[6];
                DeleteOldObjects();
            }
            Debug.WriteLine("{0}: STATUS: PlayerID={1}, TimeLeft={2}", DateTime.Now, PlayerID, TimeLeft);
        }
        private void HandleItemPacket(byte[] packetArray, uint ItemID)
        {
            #region Create variables from the packet array
            ItemType ItemType = (ItemType)packetArray[4];
            char ItemChar = (char)packetArray[5];
            byte OwnerID = packetArray[6];
            byte X = packetArray[7];
            byte Xfrac = packetArray[8];
            byte Y = packetArray[9];
            byte Yfrac = packetArray[10];
            int RotHIGH = packetArray[11];
            int RotLOW = packetArray[12];
            int Rot = RotHIGH * 256 + RotLOW;
            int Speed = (packetArray[13] - 100);
            byte BulletNum = packetArray[14];
            byte RocketNum = packetArray[15];
            #endregion

            double realX = double.Parse(string.Format("{0},{1}", X, Xfrac));
            double realY = double.Parse(string.Format("{0},{1}", Y, Yfrac));

            int realXrounded = (int)Math.Round(realX, 0);
            int realYrounded = (int)Math.Round(realY, 0);

            GameObject gameObject;
            switch (ItemType)
            {
                case ItemType.AMMOCRATE: gameObject = new AmmoCrate(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum); break;
                case ItemType.BULLET: gameObject = new NormalBullet(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum); break;
                case ItemType.ROCKET: gameObject = new RocketBullet(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum); break;
                case ItemType.WALL:
                    {
                        gameObject = new HardWall(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum);
                        int alreadyIndex = Walls.FindIndex(s => s.ItemID == gameObject.ItemID);
                        if (alreadyIndex != -1)
                        {
                            Walls[alreadyIndex] = gameObject as HardWall;
                        }
                        else
                        {
                            Walls.Add(gameObject as HardWall);
                        }
                        break;
                    }
                case ItemType.WEAKWALL:
                    {
                        gameObject = new WeakWall(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum);
                        int alreadyIndex = Walls.FindIndex(s => s.ItemID == gameObject.ItemID);
                        if (alreadyIndex != -1)
                        {
                            Walls[alreadyIndex] = gameObject as WeakWall;
                        }
                        else
                        {
                            Walls.Add(gameObject as WeakWall);
                        }
                        break;
                    }
                case ItemType.TANK:
                    {
                        gameObject = new Player(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum);
                        int alreadyIndex = Enemies.FindIndex(s => s.ItemID == gameObject.ItemID);
                        if (alreadyIndex != -1)
                        {
                            Enemies[alreadyIndex] = gameObject as Player;
                        }
                        else
                        {
                            Enemies.Add(gameObject as Player);
                        }

                        if (MyPlayerID != int.MinValue && gameObject.OwnerID == MyPlayerID)
                        {
                            if (PreviousOtherPlayerPoint == null)
                            {
                                PreviousOtherPlayerPoint = new Point((gameObject as Player).Xrounded, (gameObject as Player).Yrounded);
                                PreviousOtherPlayerPointTime = DateTime.Now;
                            }
                            else if (!(PreviousOtherPlayerPoint.X == (gameObject as Player).Xrounded &&
                                PreviousOtherPlayerPoint.Y == (gameObject as Player).Yrounded))
                            {
                                PreviousOtherPlayerPoint = new Point((gameObject as Player).Xrounded, (gameObject as Player).Yrounded);
                                PreviousOtherPlayerPointTime = DateTime.Now;
                            }
                            MyPlayer = gameObject as Player;
                            Enemies.Remove(MyPlayer);
                        }
                        break;
                    }
                default:
                    gameObject = new ErrorObject(ItemID, ItemType, ItemChar, OwnerID, realX, realY, Rot, Speed, BulletNum, RocketNum); break;
            }
            DeleteIfAlreadyContains(ItemID);
            Map[realYrounded, realXrounded].Add(gameObject);

            MainWindowObj._VIEW_AddObject(realYrounded, realXrounded, ItemChar);

            Player objPlayer = gameObject as Player;
            if (objPlayer != null && MyPlayerID != int.MinValue && objPlayer.OwnerID == MyPlayerID)
            {
                MainWindowObj._VIEW_ActualizeMyPlayerData(objPlayer);
            }
        }
        private void DeleteOldObjects()
        {
            for (int row = 0; row < Map.GetLength(0); row++)
            {
                for (int col = 0; col < Map.GetLength(1); col++)
                {
                    for (int i = Map[row, col].Count - 1; i >= 0; i--)
                    {
                        if (DateTime.Now.Subtract(Map[row, col][i].CreationDate) > new TimeSpan(0, 0, 1))
                        {
                            Map[row, col].Remove(Map[row, col][i]);
                            MainWindowObj._VIEW_RemoveObject(row, col);
                        }
                    }
                }
            }
        }
        private Point DeleteIfAlreadyContains(uint ItemID)
        {
            Point point = null;
            for (int row = 0; row < Map.GetLength(0); row++)
            {
                for (int col = 0; col < Map.GetLength(1); col++)
                {
                    int index = Map[row, col].FindIndex(s => s.ItemID == ItemID);
                    if (index >= 0)
                    {
                        Map[row, col].RemoveAt(index);
                        MainWindowObj._VIEW_RemoveObject(row, col);
                        point = new Point(col, row);
                        break;
                    }
                }
            }
            return point;
        }
    }
}