function Main() {
	var timeline = new Array(24*60);

	var sorok = document.getElementById("ta").value.split('\n');
	document.getElementById("result").innerHTML = '';
	for (var i=0; i<sorok.length; i+=2) {
		for (var z=0; z<timeline.length; z++) {
			timeline[z] = 0;
		}

		var idoErkezesek = sorok[i].split(';');
		var idoIndulasok = sorok[i+1].split(';');
		for (var j=0; j<idoErkezesek.length; j++) {
			var orapercErkezes = idoErkezesek[j].split(':');
			var indexErkezes = parseInt(orapercErkezes[0])*60+parseInt(orapercErkezes[1]);

			var orapercIndulas = idoIndulasok[j].split(':');
			var indexIndulas = parseInt(orapercIndulas[0])*60+parseInt(orapercIndulas[1]);
			
			if ( indexIndulas < indexErkezes ) {
				for (var k=indexErkezes; k<24*60; k++) {
					timeline[k] += 1;
				}
				for (var k=0; k<=indexIndulas; k++) {
					timeline[k] += 1;
				}
			} else {
				for (var k=indexErkezes; k<=indexIndulas; k++) {
					timeline[k] += 1;
				}
			}
		}

		var largest = Math.max.apply(Math, timeline);
		document.getElementById("result").innerHTML += '<DIV><STRONG>' + largest + '</STRONG><SPAN style="padding-left: 4em;">Erk.: ' + idoErkezesek + ' Ind.:' + idoIndulasok + '</SPAN></DIV>';
	}
	
}
