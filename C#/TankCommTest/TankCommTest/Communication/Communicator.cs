﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TankCommTest.Communication
{
    public static class Communicator
    {
        const string ServerIP = "10.4.11.24";
        const int ServerPort = 11111;

        const int MyPortUpperHexa = 72;
        const int MyPortLowerHexa = 54;

        static int MyPort;
        static UdpClient listener;

        static Communicator()
        {
            _Init();
        }
        private static void _Init()
        {
            MyPort = Convert.ToInt32(MyPortUpperHexa.ToString() + MyPortLowerHexa.ToString(), 16);
            listener = new UdpClient(MyPort);
        }

        public static void StartRecieving()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            byte[] data = new byte[1024];
            while (true)
            {
                data = listener.Receive(ref sender);
                Console.WriteLine(data);
                //Console.WriteLine(Encoding.ASCII.GetString(data, 0, data.Length));
                //listener.Send(data, data.Length, sender);
            }
        }
        public static void SendLOGIN()
        {
            _sendLoginOrLogout(true);
        }
        public static void SendLOGOUT()
        {
            _sendLoginOrLogout(false);
        }
        private static void _sendLoginOrLogout(bool login)
        {
            UdpClient client = new UdpClient();
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort);
            client.Connect(endPoint.Address, ServerPort);

            byte[] message = {
                255, 255, 255, 255,
                login? (byte)1 : (byte)3,
                Convert.ToByte(MyPortUpperHexa.ToString(), 16),
                Convert.ToByte(MyPortLowerHexa.ToString(), 16),
                0, 0, 0, 0, 0, 0, 0, 0, 0
            };

            client.Send(message, message.Length);

            if (login)
            {
                Console.WriteLine("LOGIN message sent.");
            }
            else
            {
                Console.WriteLine("LOGOUT message sent.");
            }
        }
    }
}