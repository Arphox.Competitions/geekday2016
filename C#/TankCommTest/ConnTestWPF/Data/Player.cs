﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnTestWPF.Data
{
    public class Player : GameObject
    {
        public int Xrounded { get; private set; }
        public int Yrounded { get; private set; }

        public Player(uint ItemID, ItemType ItemType, char ItemChar, int OwnerID, double X, double Y, int Rot, int Speed, int BulletNum, int RocketNum)
            : base(ItemID, ItemType, ItemChar, OwnerID, X, Y, Rot, Speed, BulletNum, RocketNum)
        {
            Xrounded = (int)Math.Round(X, 0);
            Yrounded = (int)Math.Round(Y, 0);
        }

        //public Player()
        //{
        //}

        public bool CanShootRocket()
        {
            return RocketNum > 0;
        }
    }
}
