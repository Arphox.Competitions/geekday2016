﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TankCommTest.Communication;

namespace ConnTestWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool NeedGraphics = false;

        ViewModel vm;
        Communicator communicator;
        DispatcherTimer reLoginTimer;

        Label[,] labelMatrix = new Label[20, 20];

        public Data.DataHandler DataHandler { get; private set; }


        public MainWindow()
        {
            InitializeComponent();
            vm = new ViewModel();
            communicator = new Communicator(vm, this, DataHandler);
            Init();
            //AllocConsole();
            DataContext = vm;
            ReLoginTimer_Init();
            LOGIN_Click(null, null);
        }
        private void Init()
        {
            DataHandler = new Data.DataHandler(this, communicator);
            displayGrid.Children.Clear();
            for (int row = 0; row < 20; row++)
            {
                for (int col = 0; col < 20; col++)
                {
                    Label label = CreateLabel(string.Empty, Brushes.Transparent);
                    Grid.SetColumn(label, col);
                    Grid.SetRow(label, row);
                    displayGrid.Children.Add(label);
                    labelMatrix[row, col] = label;
                }
            }
            GasHoldTimer = new DispatcherTimer();
            GasHoldTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            GasHoldTimer.Tick += GasHoldTimer_Tick;
        }
        private void ReLoginTimer_Init()
        {
            reLoginTimer = new DispatcherTimer();
            reLoginTimer.Interval = new TimeSpan(0, 0, 1);
            reLoginTimer.Tick += reLoginTimer_Tick;
            reLoginTimer.Start();
        }
        private void reLoginTimer_Tick(object sender, EventArgs e)
        {
            if (!communicator.FirstLoginDone)
                return;

            if (DateTime.Now.Subtract(communicator.LastTimeDataRecieved) >= new TimeSpan(0, 0, 1))
            {
                Debug.WriteLine("{0}: Not recieving data, performing LOGIN", DateTime.Now);
                LOGIN_Click(null, null);
            }
        }
        private void LOGIN_Click(object sender, RoutedEventArgs e)
        {
            Init();
            communicator.SendLOGIN();
        }
        private void LOGOUT_Click(object sender, RoutedEventArgs e)
        {
            communicator.SendLOGOUT();
        }


        // DataHandler used methods
        public void _VIEW_RemoveObject(int row, int col)
        {
            if (!NeedGraphics)
                return;
            labelMatrix[row, col].Content = string.Empty;
            labelMatrix[row, col].Background = Brushes.Transparent;
        }
        public void _VIEW_AddObject(int row, int col, char ItemChar)
        {
            if (!NeedGraphics)
                return;

            switch (ItemChar)
            {
                case 'W': // kemény fal
                    {
                        labelMatrix[row, col].Content = string.Empty;
                        labelMatrix[row, col].Background = Brushes.Black;
                        break;
                    }
                case 'V': // szétlőhető fal
                    {
                        labelMatrix[row, col].Content = string.Empty;
                        labelMatrix[row, col].Background = Brushes.Gray;
                        break;
                    }
                case 'O': // töltényláda
                case 'R': // rakéta
                case 'B': // hétköznapi töltény
                    {
                        labelMatrix[row, col].Content = ItemChar;
                        labelMatrix[row, col].Background = Brushes.Transparent;
                        break;
                    }
                default: // egy tank
                    {
                        labelMatrix[row, col].Content = ItemChar;
                        labelMatrix[row, col].Background = Brushes.Yellow;
                        break;
                    }
            }
        }
        public void _VIEW_ActualizeMyPlayerData(Data.Player player)
        {
            if (!NeedGraphics)
                return;

            label_PlayerID.Content = player.OwnerID;
            label_Rot.Content = player.Rot;
            label_Speed.Content = player.Speed;
            label_Pos.Content = string.Format("X:{0}, Y:{1}", player.X, player.Y);
            label_BulletNum.Content = player.BulletNum;
            label_RocketNum.Content = player.RocketNum;
        }

        private Label CreateLabel(string Content, Brush backgroundColor)
        {
            Label label = new Label();
            label.Content = Content;
            label.HorizontalContentAlignment = HorizontalAlignment.Center;
            label.VerticalContentAlignment = VerticalAlignment.Center;
            label.FontSize = 20;
            label.FontWeight = FontWeights.Bold;
            label.Background = backgroundColor;
            return label;
        }


        #region Keyboard handling
        DispatcherTimer GasHoldTimer;
        private void GasHoldTimer_Tick(object sender, EventArgs e)
        {
            if (!Keyboard.IsKeyDown(Key.RightShift))
            {
                //communicator.SendStopCommand();
                //GasHoldTimer.Stop();
                communicator.SendCommand(CommandType.SETSPEED, 100);
            }
        }
        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //switch (e.Key)
            //{
            //    case Key.W: // ↑
            //        communicator.SendCommand(CommandType.SETROTATION, 27); break;
            //    //case Key.E: // ↑ →
            //    //    communicator.SendCommand(CommandType.SETROTATION, 32); break;
            //    case Key.D: // → 
            //        communicator.SendCommand(CommandType.SETROTATION, 0); break;
            //    //case Key.C: // → ↓
            //    //    communicator.SendCommand(CommandType.SETROTATION, 5); break;
            //    case Key.S: // ↓
            //        communicator.SendCommand(CommandType.SETROTATION, 9); break;
            //    //case Key.Y: // ↓ ←
            //    //    communicator.SendCommand(CommandType.SETROTATION, 14); break;
            //    case Key.A: // ←
            //        communicator.SendCommand(CommandType.SETROTATION, 18); break;
            //    //case Key.Q: // ← ↑
            //    //    communicator.SendCommand(CommandType.SETROTATION, 22); break;
            //    case Key.RightShift: // Gas
            //        communicator.SendCommand(CommandType.SETSPEED, 200);
            //        GasHoldTimer.Start();
            //        Debug.WriteLine("{0}: GAS sent from keystroke", DateTime.Now);
            //        break;
            //    case Key.Space:
            //        communicator.SendCommand(CommandType.SHOOTBULLET, 0);
            //        break;
            //    case Key.RightAlt:
            //        communicator.SendCommand(CommandType.SHOOTROCKET, 0);
            //        break;
            //}
        }
        #endregion
        #region Misc
        [DllImport("kernel32")]
        static extern bool AllocConsole();
        #endregion

        private void TurnOnGraphics_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
