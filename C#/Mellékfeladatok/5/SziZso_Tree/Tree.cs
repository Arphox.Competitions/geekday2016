﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SziZso_Tree
{
    class Tree
    {
        public static int KezdoSzam { get; set; }
        public static List<int> KezdoSzamOsztoi { get; set; }


        public Node RootNode { get; set; }
        public Tree(Tree Parent, Node Node)
        {
            this.RootNode = Node;
        }



        public static List<Tree> AutoGenerateStartingTrees()
        {
            List<Tree> trees = new List<Tree>();
            foreach (int oszto in KezdoSzamOsztoi)
            {
                trees.Add(new Tree(null, new Node(null, oszto, null, 1)));
            }
            return trees;
        }

        public static List<int> GetOsztok_ÖnmagátKivéve(int szam)
        {
            List<int> osztok = new List<int>();
            for (int i = 1; i < szam; i++)
            {
                if (szam % i == 0)
                {
                    osztok.Add(i);
                }
            }
            return osztok;
        }

        public static void DepthFirstTraversal(Tree tree)
        {
            foreach (Node node in tree.RootNode.Children)
            {
                _depthFirstTraversal(node);
            }
            Console.WriteLine(tree.RootNode);
        }
        private static void _depthFirstTraversal(Node node)
        {
            if (node.Children.Count == 0)
            {
                for (int i = 0; i < node.Depth - 1; i++)
                {
                    Console.Write("\t");
                }
                Console.WriteLine(node);
            }
            else
            {
                foreach (Node child in node.Children)
                {
                    _depthFirstTraversal(child);
                }
                for (int i = 0; i < node.Depth - 1; i++)
                {
                    Console.Write("\t");
                }
                Console.WriteLine(node);
            }
        }
    }
}