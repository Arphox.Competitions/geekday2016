﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnTestWPF.Data
{
    class NormalBullet : Bullet
    {
        public NormalBullet(uint ItemID, ItemType ItemType, char ItemChar, int OwnerID, double X, double Y, int Rot, int Speed, int BulletNum, int RocketNum)
            : base(ItemID, ItemType, ItemChar, OwnerID, X, Y, Rot, Speed, BulletNum, RocketNum)
        {
        }
    }
}
