A little more detailed info about how the bonuses will work:

* bonusSpeed => If achieved,then the tank is 25% faster (e.g. the 100 speed if you have the bonus is the same as 125 would be without the bonus)
* bonusBulletRegeneration => Normal tanks receive +1 bullets once in every 60 seconds. With this bonus, once in every 40 seconds.
	(during development time: once in every 10 seconds)
* bonusRocketRegeneration => Normal tanks receive +1 rockets once in every 60 seconds. With this bonus, once in every 40 seconds.
	(during development time: once in every 11 seconds)
* bonusBulletSpeed => normal bullets go with speed of 105. With this bonus bullet speed is 125
* bonusRocketSpeed => normal rockets go with speed of 70. With this bonus, rocket speed is 90
